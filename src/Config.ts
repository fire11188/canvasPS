import { Layer } from "./Layer";
/**
 * 历史记录对象
 */
export class History {
    public title: string
    public data: any
    public layer: Layer = new Layer
    constructor(title: string, data: any) {
        this.title = title
        this.data = data
    }
}
/**
 * 自定义数据表
 */
export class ArrayList<T>{
    /**
     * 数据变化监听
     */
    public OnChange: any = null
    private __list: Array<T> = new Array
    /**
     * 长度
     */
    get length() {
        return this.__list.length
    }
    /**
     * 原始数据
     */
    get List() {
        return this.__list
    }
    /**
     * 添加元素
     * @param items 
     */
    push(...items: any[]): number {
        const num = this.__list.push(...items)
        this.change();
        return num
    }
    /**
     * 返回第一个元素，并移除列表
     * @returns 
     */
    shift() {
        const num = this.__list.shift();
        this.change();
        return num
    }
    /**
     * 清空
     */
    clear() {
        this.__list = []
        this.change()
    }
    /**
     * 根据索引取一个
     * @param index 
     * @returns 
     */
    getOne(index: number) {
        return this.__list[index]
    }
    change() {

        if (typeof this.OnChange === 'function') {
            this.OnChange()
        }
    }
}
/**
 * 画板状态配置
 */
export const Config = {
    /*
    0-正常 1-魔术棒 2-画笔 3-橡皮擦 4-裁剪 5-调色
     6-移动画布 7-缩放画布 8-填充 9-铅笔 10-画笔抠图
     11-多边形选择 12--拾色器
    */
    mode: 0,
    pen: {
        color: 'rgba(255,0,0,1)',
        colors: ['255', '0', '0', '1'],
        size: 10,
        status: 0,
    },
    canvas: {
        scale: 100,
        show: true,
        backgruond: 'none',
        x: 0,
        y: 0,
    },
    mouse: {
        status: 1,//0-按下 1-松开 2-移动
        down: {
            x: 0,
            y: 0,
        },
        up: {
            x: 0,
            y: 0,
        },
        move: {
            x: 0,
            y: 0,
        },
        cutBox: {
            status: 1,//0-按下 1-松开 2-移动 3-调整
            down: {
                x: 0,
                y: 0,
                ox: 0,
                oy: 0,
            },
            up: {
                x: 0,
                y: 0,
            },
            move: {
                x: 0,
                y: 0,
            },
        },
    },
    touch: {
        canvas: {
            status: 1,//0-按下 1-松开 2-移动
            down: {
                x: 0,
                y: 0,
            },
            up: {
                x: 0,
                y: 0,
            },
            move: {
                x: 0,
                y: 0,
            },
        },
        timer: null,
    },
    elements: {
        cutBox: document.createElement('div'),
    },
    history: new ArrayList<History>,
    historyMax: 16,
    historyIndex: 0,
}
